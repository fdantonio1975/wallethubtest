# WalletHub test #
by Fulvio D'Antonio
fulvio.dantonio@gmail.com

## Description of the dataset 
The data set contains 100,000 instances.
There are 304 input features, labeled x001 to x304.
The target variable is labeled y.

## Assumptions 

The value of the target variable is an integer in the interval [300,839].
The prediction of the y variable will be addressed as a regression problem.

## Technologies

The software for training and prediction has been been developed using Python3.6 and the sklearn,scipy, pandas stack.
A docker image has been created and pushed on dockerhub to allow to run the code with minimal effort.
The versions of the libraries installed are the following (output of 'pip freeze' command):

* numpy==1.12.1
* pandas==0.19.2
* python-dateutil==2.6.0
* pytz==2017.2
* scikit-learn==0.18.1
* scipy==0.19.0
* six==1.10.0
* sklearn==0.0

## Preprocessing 

Missing values have been filled with zeros.
The features have been scaled and centered by using StandardScaler (computing z-scores): http://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.StandardScaler.html

However not all the approaches tested during the training phase are sensitive to feature scales. In particular the winning approach performs identically with or without feature scaling so feature scaling will not be applied to the test dataset (without loss of performance).

Note: "x001" appears to be a sort of id (10^5 distinct values) and could be probably safely removed from the dataset. However the impact on the performance of the various approaches with r without this variable is minimal.

## Approaches to regression

Several approaches have been tested:

* **Linear regression** with and without regularization: (http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html)
* **SVR** (SVM adapted for regression): http://scikit-learn.org/stable/modules/generated/sklearn.svm.SVR.html#sklearn.svm.SVR
* **Random Forest Regression** (varying the number of estimators): http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html
* **MultiLayer Perceptron Regression**: http://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPRegressor.html
* **AdaBoostRegressor** by using as a base estimator one of the previously cited regressors

The best performing approach has been Random Forest Regression.
The data has been split into a Traing Set (80%) and Test Set (20%).
The scores obtained on the Test Set are the following:

* **RMSE: 29.2994854477**
* **Accuracy: 0.15415**

Accuracy has been measured as the percentage of correct predictions (where "correct" means |prediction-truth|<=3)



#Installation/Run

The easiest way (recommended) to run the prediction on your csv test file is the following:

* move to the directory containing the file: **cd <directory containing the csv test file>**
* create a docker container: **docker run --rm -it -v $PWD:/mount -w /WalletHubTest fulviodan/wallethub bash**
* now you're inside the container in interactive mode and the csv file is available under the "/mount" directory. Proceed and launch: **./predict /mount/<csv file name>**
* The scores (RMSE and Accuracy) are printed to stdout and the values of predictions are outputted to "output.predictions" in the same directory.
* If you want to copy "output.predictions" to host the easiest way is to copy it under the "/mount" directory that is shared with the host

The alternative way (not recommended) requires to setup a python/scikit/pandas environment:

* install python3.6
* clone the project: git clone https://fdantonio1975@bitbucket.org/fdantonio1975/wallethubtest.git

* install the required libraries: pip install pandas; pip install sklearn; pip install scikit

* Move to cloned directory root: cd <...>/WalletHubTest
* launch predict: ./predict.sh <csv test file>

## Hardware configuration/Resources estimation 

The machine used for test is an Intel QuadCore (8 threads) and 8 GB ram.
To generate the predictions for 10^5 samples requires ~8 seconds (by using all available cores) with a RAM peak of 500mb-1Gb.