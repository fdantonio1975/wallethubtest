from wht.config.Sources import loadClassifer, toXy
from wht.config import Sources
from sklearn.metrics.regression import mean_squared_error
from sklearn.preprocessing.data import StandardScaler
import numpy
import pandas
import sys

def printScoresAndSavePredictions(testPath):
    kl=loadClassifer()
    df= pandas.read_csv(testPath).fillna(value=0)
    #df=df.sample(10000)
    X,y=toXy(df)
    
    
    predictions=kl.predict(X)
    
    print("RMSE",mean_squared_error(y,predictions)**.5)
    print("Accuracy",(numpy.abs(predictions-y)<=3).sum()/len(y))
    numpy.savetxt("output.predictions",predictions,delimiter=",",fmt="%.0f")
    print("Predictions written to 'output.predictions'")
if __name__=="__main__":
    if len(sys.argv)!=2:
        print("Usage: ./predict.sh <csv file>")
        exit()
    printScoresAndSavePredictions(sys.argv[1])