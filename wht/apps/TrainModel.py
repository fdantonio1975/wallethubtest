from wht.config import Sources
from sklearn.preprocessing.data import StandardScaler
from sklearn.ensemble.forest import ExtraTreesClassifier, RandomForestRegressor,\
    RandomTreesEmbedding
from sklearn.feature_selection.from_model import SelectFromModel
from sklearn.model_selection._validation import cross_val_score
from sklearn.ensemble.weight_boosting import AdaBoostRegressor
from sklearn.model_selection._split import train_test_split

from sklearn.metrics.regression import mean_squared_error
from sklearn.externals import joblib
from wht.config.Sources import toXy
import numpy
from sklearn.ensemble.gradient_boosting import GradientBoostingRegressor

df=Sources.data().fillna(value=0)
#df=df.sample(10000)
X,y=toXy(df)
#X=StandardScaler().fit_transform(X)
X1,X2,y1,y2=train_test_split(X,y,test_size=.2)
lr=RandomForestRegressor(n_estimators=20,n_jobs=-1)

lr.fit(X1,y1)

predictions=lr.predict(X2)
print("RMSE",mean_squared_error(y2,predictions)**.5)
accuracy=(numpy.abs(y2-predictions)<=3).sum()/len(y2)
print("Accuracy",accuracy)
joblib.dump(lr,"../../data/model.bin")