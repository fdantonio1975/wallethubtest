import pandas as pd
from sklearn.externals import joblib
from sklearn.preprocessing.data import StandardScaler
def data(n=None):
    if n:
        return pd.read_csv("../../data/dataset_00_with_header.csv",nrows=n)
    else:
        return pd.read_csv("../../data/dataset_00_with_header.csv")

def toXy(df):    
    X=df[[x for x in df.columns if x!='y']]
    return X,df.y

def loadClassifer():
    return joblib.load("./data/model.bin")